#                   __ _          __ _     _     
#                  / _(_)        / _(_)   | |    
#   ___ ___  _ __ | |_ _  __ _  | |_ _ ___| |__  
#  / __/ _ \| '_ \|  _| |/ _` | |  _| / __| '_ \ 
# | (_| (_) | | | | | | | (_| |_| | | \__ \ | | |
#  \___\___/|_| |_|_| |_|\__, (_)_| |_|___/_| |_|
#                         __/ |                  
#                        |___/                   
# 
# Pablo Emilio Escobar Gavíria (C) 2020
#

# Sources environment variables
source "$HOME/.local/bin/exports.sh"

# Configure the PATH variable
set PATH "/usr/local/sbin" \
         "/usr/local/bin" \
         "/usr/sbin" \
         "/usr/bin" \
         "/sbin" \
         "/bin" \
         "/usr/games" \
         "/usr/local/games" \
         "$HOME/.local/bin" \
         "$CARGO_HOME/bin" \
         "$GOPATH/bin" \
         "$DENO_INSTALL/bin" \
         "/usr/local/wabt/bin" \
         "$XDG_DATA_HOME/gap-4.11.0" \
         "$XDG_DATA_HOME/emsdk" \
         "$XDG_DATA_HOME/npm/bin" \
         "$XDG_DATA_HOME/emsdk/upstream/emscripten" \
         "$XDG_DATA_HOME/mitra" \
         "$XDG_DATA_HOME/tmux" \
         "$XDG_DATA_HOME/julia-1.5.3/bin" \
         "$XDG_DATA_HOME/fzf/bin" \
         /usr/local/share/*/{include,bin,lib}

# Configure the MANPATH variable
set MANPATH "/usr/share/man" \
            "/usr/local/man" \
            "$XDG_DATA_HOME/man" \
            "$XDG_DATA_HOME/fzf/man" \

# Abbreviations
abbr vim     'nvim'
abbr sc      'sc-im'
abbr weather 'curl https://wttr.in/'
abbr mutt    'neomutt'

# Aliases
alias canto   'canto-curses'
alias less    'less -Rf'
alias wget    'wget --hsts-file="$XDG_CACHE_HOME/wget-hsts"'
alias gdb     'gdb -nh -x "$XDG_CONFIG_HOME/gdb/init"'
alias weechat 'weechat -d "$XDG_CONFIG_HOME/weechat"'
alias lynx    'lynx -cfg="$XDG_CONFIG_HOME/lynx/lynx.cfg"'
alias git     'env TZ=-0000 git' # Hide my timezone when commiting
alias abook   'abook --config "$XDG_CONFIG_HOME"/abook/abookrc --datafile "$XDG_DATA_HOME"/abook/addressbook'
alias neomutt 'env TZ=-0000 torify neomutt' # Make neomutt stealthy

function fish_greeting
    set_color yellow
    fortune | cowsay -f none
    set_color normal
    fish_logo red ff743d yellow
    printf '\n'
end

function fish_prompt
    # Formatted pwd
    set dir (string replace "$HOME" '~' (pwd))

    # Show fancy prompt if the screen is big enought
    if test (math (string length "$dir") + 4) -lt (math (tput cols) \* 0.50)
      set_color --bold white
      printf "["

      # Print the username
      set_color --bold green
      printf "%s@%s" (whoami) (hostname)

      # Print the current directory
      set_color --bold blue
      printf " %s" "$dir"

      # Print the current git branch (if inside a repo)
      set_color --bold cyan
      __fish_git_prompt

      set_color --bold white
      printf ']' 
    else
      set_color --bold blue
      printf '%s ' "$dir"
      set_color --bold white
    end

    if [ $status -ne 0 ]
        set_color --bold red
    end

    if test "$USER" = root
      printf '# '
    else
    	printf '$ '
    end
      
    set_color normal
end

