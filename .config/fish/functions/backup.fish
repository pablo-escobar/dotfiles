function backup -d "Backup my relevant data in my backup drive."
  set date (date '+%Y-%m-%d')

  # Backup the most important directories into an archive named after
  # the machine this script is currently running on:
  borg create                                                          \
    --verbose                                                          \
    --filter AME                                                       \
    --list                                                             \
    --stats                                                            \
    --show-rc                                                          \
    --compression lz4                                                  \
    --exclude-caches                                                   \
    --exclude-from "$HOME/.local/share/excludefile"                    \
    "/media/pablo/Seagate Backup Plus Drive/Backups/::$hostname-$date" \
    "$HOME/Archive"                                                    \
    "$HOME/Documents"                                                  \
    "$HOME/Passwords"                                                  \
    "$HOME/Pictures"                                                   \
    "$HOME/Videos"                                                     \
    "$HOME/Emulation"                                                  \
    "$HOME/VirtualBox VMs"                                             \
    "$HOME/.config"                                                    \
    "$HOME/.local/share/excludefile"                                   \
    "$HOME/.local/share/texmf"                                         \
    "$HOME/.local/bin/exports.sh"                                      \

end

