function config -d "A simple configuration manager"
  # Calls git in the appropriate repository
  function _call_git
    git --git-dir="$HOME/Documents/dotfiles" --work-tree="$HOME" $argv
  end

  argparse u/update=+ a-add=+ r-rm=+ l/list g-log s/status d/diff f-edit-self e/edit -- $argv

  # List the dotfiles specified in configurations list
  if test -n "$_flag_list"
    string replace --all "$HOME" '~' (cat ~/.local/share/configs.list)
    return
  end

  # Call git log
  if test -n "$_flag_log"
    _call_git log
    return
  end

  # Show the configuration files that have been modified
  if test -n "$_flag_status"
    _call_git status >> /tmp/config-git-status

    echo 'modified files:'
    cat /tmp/config-git-status \
      | grep 'modified' \
      | awk '{ print $2 }' \
      | xargs -r realpath \
      | string replace "$HOME" '~' \
      | xargs -r -n 1 printf '    %s\n'

    echo 'deleted files:'
    cat /tmp/config-git-status \
      | grep 'deleted' \
      | awk '{ print $2 }' \
      | xargs -r realpath \
      | string replace "$HOME" '~' \
      | xargs -r -n 1 printf '    %s\n'

    rm /tmp/config-git-status
    return
  end

  # Call git diff
  if test -n "$_flag_diff"
    _call_git diff
    return
  end

  if test -n "$_flag_edit_self"
    $EDITOR (type -p config)
  end

  # Add files to the list of dotfiles
  for item in $_flag_add
    echo "Adding '$item' to the list of dotfiles"
    echo (realpath "$item") >> ~/.local/share/configs.list
  end

  # Remove files from the list of dotfiles
  for item in $_flag_rm
    echo "Removing '$item' from the list of dotfile"

    # Set is used because 'item' may be a directory
    set pattern (realpath "$item")
    set pattern (string replace --all '/' '\/' "$pattern")
    set pattern (string replace --all '.' '\.' "$pattern")
    sed -e "/^$pattern/d" -i ~/.local/share/configs.list
    _call_git rm "$item" --cached -r
  end

  # Update the repository and it's remotes
  if test -n "$_flag_update"
    # Add the relevant files to the repo
    cat ~/.local/share/configs.list \
      | xargs git --git-dir="$HOME/Documents/dotfiles" --work-tree="$HOME" add

    for msg in $_flag_update
      printf '%s\n\n' "$msg" >> /tmp/config-git-message
    end

    # Commit the changes
    and _call_git commit -F /tmp/config-git-message

    # Return if there's an error
    or return $status

    # Push the changes to all remotes
    for remote in (_call_git remote -v | awk '{ print $1 }' | uniq)
      _call_git push "$remote" master
    end

    rm /tmp/config-git-message
  end
end

