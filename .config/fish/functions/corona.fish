#   __ _     _                                          
#  / _(_)   | |                                         
# | |_ _ ___| |__ ______ ___ ___  _ __ ___  _ __   __ _ 
# |  _| / __| '_ \______/ __/ _ \| '__/ _ \| '_ \ / _` |
# | | | \__ \ | | |    | (_| (_) | | | (_) | | | | (_| |
# |_| |_|___/_| |_|     \___\___/|_|  \___/|_| |_|\__,_|
#                                                      
# Pablo Emilio Escobar Gavíria (C) 2020
# You are free (as in freedom) to do whatever you please with this!

# Filters the cached CSV and adds a header to it
function __corona_select_csv -a country
    echo '🚩️ Country, 😷️ Cases, 🤒️ New Cases, 💀️ Deaths, 😰️ New Deaths'

    if test (string lower "$country") = all
        cat "$HOME/.cache/corona.csv"
    else
        grep -Ei "^\"?($country|World)" "$HOME/.cache/corona.csv"
    end
end

function corona -a country -d "Track corona-virus cases from fish-shell. 😷️"
    if test -z "$country"
        if test -f "$HOME/.cache/country.name"
            set country (cat "$HOME/.cache/country.name" )
        else
            set country_code (curl -s https://ipinfo.io/country/)
            set country (curl -s "https://restcountries.eu/rest/v2/alpha/$country_code/" | jq '.name' | sed 's/"//g')
            echo "$country" > "$HOME/.cache/country.name" 
        end
    end

    # Update the cache if it doesn't exists or if it hasn't been modified in
    # over a day
    set date (date +%Y-%m-%d)
    not test -f "$HOME/.cache/corona.csv"
    or test (stat -c %y "$HOME/.cache/corona.csv" | cut -d' ' -f1) != "$date"

    if test "$status" -eq 0
        curl -s https://corona-stats.online/\?format=json \
            | jq -r '.data + [.worldStats] 
                        | .[] 
                        | [.country,.cases,.todayCases,.deaths,.todayDeaths] 
                        | @csv' \
            > "$HOME/.cache/corona.csv"
    end

    __corona_select_csv "$country" | csvlook -d ',' -e UTF-8
end

