function mkimg -a file -d "Creates a MS-DOS compatible disk image"
  dd if=/dev/zero of="$file" count=1440 bs=1k
  /sbin/mkfs.msdos "$file"
end

