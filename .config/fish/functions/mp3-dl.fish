function mp3-dl -a url
  set input_dir (mktemp -d)

  set title (youtube-dl --get-title "$url")
  set format (youtube-dl -F "$url" | sed '/audio only/!d' | fzf --prompt='Which format do you prefer? ' | awk '{ print $1 }')
  youtube-dl -f "$format" "$url" -o "$input_dir/input.%(ext)s"

  if test "$format" = 'mp3'
    and mv $input_dirp/input.* "./$title.mp3"
  else
    and ffmpeg -i $input_dir/input.* -c:a libmp3lame -ac 2 -q:a 2 "./$title.mp3"
  end

  rm -rf "$input_dir"
end

