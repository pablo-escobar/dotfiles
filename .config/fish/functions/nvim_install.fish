function nvim_install -a url -d "install vim plugins from git repositories"

    for repo in $argv
        set sub_dir (echo "$url" | grep -o '[^/]*/[^/]*$' | grep -o '^[^/]*')
        set pack (echo "$url" | grep -o '[^/]*$' | grep -o '^[^.]*')
        set install_dir "$HOME/.local/share/nvim/site/pack/$sub_dir/start/$pack"

        if test -d "$install_dir"
            # Update the repository
            set pwd (pwd)
            cd "$install_dir"
            git pull
            cd "$pwd"
        else
            # Clone the repository
            git clone "$url" "$install_dir"
        end
    end
end

