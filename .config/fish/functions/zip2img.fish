#      _        _____ _                 
#     (_)      / __  (_)                
#  _____ _ __  `' / /'_ _ __ ___   __ _ 
# |_  / | '_ \   / / | | '_ ` _ \ / _` |
#  / /| | |_) |./ /__| | | | | | | (_| |
# /___|_| .__/ \_____/_|_| |_| |_|\__, |
#       | |                        __/ |
#       |_|                       |___/ 
#
# Converts ZIP archives to MS-DOS compatible disk images (virtual floppies)
#
# Pablo (C) 2020

function zip2img -a zip img -d "Converts ZIP archives to MS-DOS compatible disk images (virtual floppies)"
    set dir (mktemp -d)

    if test -z "$img"
      set fname (echo "$zip" | sed 's/\.[^.]*$//g')
      set fdir (dirname "$zip")
      set img "$fdir/$fname.img"
    end

    # Extract the contents of the archive to a temporary directory
    unzip "$zip" -d "$dir/"

    # Create the disk image and copy the contents of the archive to it
    dd if=/dev/zero of="$img" count=1440 bs=1k
    /sbin/mkfs.msdos "$img"
    mcopy -i "$img" "$dir/"* ::/
   
    # Delete the temporary directory
    rm "$dir/" -rf
end

