"  _       _ _         _           
" (_)     (_) |       (_)          
"  _ _ __  _| |___   ___ _ __ ___  
" | | '_ \| | __\ \ / / | '_ ` _ \ 
" | | | | | | |_ \ V /| | | | | | |
" |_|_| |_|_|\__(_)_/ |_|_| |_| |_|
" 
" Pablo (C) 2020
"

" Turn on syntax highlighting
syntax on
filetype indent plugin on

" Configure keybindings
let mapleader = " "
"Maps <Leader><Leader> to 'next window'
map <Leader><Leader> <C-W>w
map <Leader><CR>     :NERDTreeToggle<CR>

" Configure tabs so that tabs are expanded to 2 spaces
set tabstop=2 softtabstop=0 expandtab shiftwidth=2 smarttab

" Set the column-cap
set colorcolumn=80
set textwidth=79

" Highlight the cursor line
set cursorline

" Set up the colorscheme
colo snazzy
let g:SnazzyTransparent = 1
let g:lightline = {'colorscheme': 'snazzy',}

" Turn on the transparent background
hi Normal ctermbg=NONE guibg=NONE

" Fix the highlighting of underlined text
hi Underlined cterm=bold,underline gui=bold,underline ctermbg=none guibg=none

" Highlight tyṕos in red
hi clear SpellBad
hi SpellBad cterm=underline gui=underline guifg=#ff5f5f

" Set up line numbering
hi LineNr cterm=bold gui=bold
hi CursorLineNr cterm=bold gui=bold ctermfg=White guifg=White
set relativenumber
set number

" Configure the directory tto store the undo files
set dir=~/.vimswap//,/var/tmp//,/tmp//,.
set undofile                        " Save undos after file closes
set undodir=$HOME/.config/nvim/undo " Where to save undo histories
set undolevels=1000                 " How many undos
set undoreload=10000                " Number of lines to save for undo

" Enabling mouse support
set mouse=a

" Auto-update a file when it changes externally
set autoread
au CursorHold * checktime

" Configure the status line
set noshowmode

" Disable the arrow keys
noremap <Up>      <Nop>
noremap <Down>    <Nop>
noremap <Left>    <Nop>
noremap <Right>   <Nop>
noremap <S-Left>  <Nop>
noremap <S-Right> <Nop>

""" Language-specific stuff

" Highlight TiKz files as LaTeX
au BufNewFile,BufRead *.tikz,*.tex        set ft=tex

" Highlight .thtml files as HTML
au BufNewFile,BufRead *.thtml             set ft=html

" Highlight .tcss files as HTML
au BufNewFile,BufRead *.tcss              set ft=css

" Highlight .wat files as WebAssembly text format
au BufNewFile,BufRead *.wat               set ft=wast

" Correctly highlight fish scripts
au BufNewFile,BufRead *.fish              set ft=fish

" Read .pl files as Prolog (not Perl).
au BufNewFile,BufRead *.pl                set ft=prolog

" Read .m and .mathematica as Mathematica files
au BufNewFile,BufRead *.m,*.mathematica   set ft=mma

" Read .g and .gap files as GAP source files
au BufRead,BufNewFile *.g,*.gi,*.gd,*.gap set ft=gap 

" Supress word wrapping when reading Python source files
au BufNewFile,BufRead *.py                set textwidth=0

