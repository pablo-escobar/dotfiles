from ranger.api.commands import Command
from os.path import join, expanduser

class empty(Command):
    """:empty

    Empties the trash directory 
    """

    def execute(self):
        self.fm.run("gio trash --empty")

class rename(Command):
    """:rename <destination>

    Renames a file
    """

    def execute(self):
        source = self.fm.thisfile
        destination = join(self.fm.thisdir.path, expanduser(self.rest(1)))
        destination = destination.strip()

        if destination:
            self.fm.run("mv '%s' '%s'" % (source, destination))
        else:
            self.fm.notify("Cancelled", bad=True)

