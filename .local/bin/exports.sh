#!/bin/sh
#                             _             _     
#                            | |           | |    
#   _____  ___ __   ___  _ __| |_ ___   ___| |__  
#  / _ \ \/ / '_ \ / _ \| '__| __/ __| / __| '_ \ 
# |  __/>  <| |_) | (_) | |  | |_\__ \_\__ \ | | |
#  \___/_/\_\ .__/ \___/|_|   \__|___(_)___/_| |_|
#           | |                                   
#           |_|                                   
# 
# File that contains all environmet variables I use. This can be sourced from,
# bash, fish, sh, zsh, etc.

# Configre XDG stuff
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"

# =========================== Programming Languages ==========================
# Rust
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"

# Python
export PYTHONSTARTUP="$XDG_CONFIG_HOME/python/init.py"

# Ruby
export GEM_HOME="$XDG_DATA_HOME/gem"
export GEM_SPEC_CACHE="$XDG_CACHE_HOME/gem"
export BUNDLE_USER_CONFIG="$XDG_CONFIG_HOME/bundle"
export BUNDLE_USER_CACHE="$XDG_CACHE_HOME/bundle"
export BUNDLE_USER_PLUGIN="$XDG_DATA_HOME/bundle"
export DEBIAN_DISABLE_RUBYGEMS_INTEGRATION="true"

# Libraries for the Go programming language
export GOPATH="$XDG_DATA_HOME/go"

# Racket
export PLTUSERHOME="$XDG_DATA_HOME/racket"

# JS
export DENO_INSTALL="$XDG_DATA_HOME/deno"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export NODE_REPL_HISTORY="$XDG_DATA_HOME"/node_repl_history

# Wasmtime
export WASMTIME_HOME="$XDG_DATA_HOME/wasmtime"

# Julia
export JULIA_DEPOT_PATH="$XDG_DATA_HOME/julia:$JULIA_DEPOT_PATH"
export JULIA_HISTORY="$XDG_CONFIG_HOME/julia/history"

# SageMath
export DOT_SAGE="$XDG_CONFIG_HOME/sage"

# Elm
export ELM_HOME="$XDG_CACHE_HOME/elm"

# ================================ Programs ==================================

# Bash history
export HISTFILE="$XDG_DATA_HOME/bash/history"

# gpg
export GNUPGHOME="$XDG_DATA_HOME/gnupg"

# readline
export INPUTRC="$XDG_CONFIG_HOME/readline/inputrc"

# less history cache
export LESSKEY="$XDG_CONFIG_HOME/less/lesskey"
export LESSHISTFILE="$XDG_CACHE_HOME/less/history"

# wget
export WGETRC="$XDG_CONFIG_HOME/wgetrc"

# LLVM
export LLVM_CONFIG="/usr/bin/llvm-config-10"

# TeXLive
export TEXMFHOME="$XDG_DATA_HOME/texmf"
export TEXMFVAR="$XDG_CACHE_HOME/texlive/texmf-var"
export TEXMFCONFIG="$XDG_CONFIG_HOME/texlive/texmf-config"

# WeeChat
export WEECHAT_HOME="$XDG_CONFIG_HOME/weechat"

# notmuch
export NOTMUCH_CONFIG="$XDG_CONFIG_HOME"/notmuch/notmuchrc
export NMBGIT="$XDG_DATA_HOME"/notmuch/nmbug

# pass(1)
export PASSWORD_STORE_DIR="$XDG_CONFIG_HOME"/password-store

# Jupyter notebook
export IPYTHONDIR="$XDG_CONFIG_HOME"/jupyter
export JUPYTER_CONFIG_DIR="$XDG_CONFIG_HOME"/jupyter

# mplayer(1)
export MPLAYER_HOME="$XDG_CONFIG_HOME"/mplayer

# aspell
export ASPELL_CONF="per-conf $XDG_CONFIG_HOME/aspell/aspell.conf; personal
$XDG_CONFIG_HOME/aspell/en.pws; repl $XDG_CONFIG_HOME/aspell/en.prepl; personal
$XDG_CONFIG_HOME/aspell/pt_BR.pws; repl $XDG_CONFIG_HOME/aspell/pt_BR.prepl"

# isync
export MBSYNCRC="$XDG_CONFIG_HOME"/isync/mbsyncrc

# Default programs
export TERMINAL="alacritty"
export EDITOR="nvim"
export GIT_EDITOR="$EDITOR"
export VISUAL="$EDITOR"
export PAGER="less"

